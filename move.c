/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 14:57:05 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/14 14:57:06 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int			move_pos(t_room *h)
{
	while (h)
	{
		if (h->pop)
			if (h->moveb == 1)
				return (1);
		h = h->next;
	}
	return (0);
}

char		*iterate_move(t_room *h, t_room *r)
{
	char	**slt;
	int		n;
	int		b;

	slt = ft_strnsplit(r->links, ' ', &n);
	while (h)
	{
		b = n + 1;
		while (--b)
		{
			if (ft_strcmp(h->id, slt[b - 1]) == 0)
				if ((h->pop <= 0 && h->weight < r->weight) || h->type == 2)
				{
					if (h->type == 2 || (h->moveb = 1) == 0)
						h->pop++;
					else
						h->pop = r->pop;
					ft_free_array(slt, n);
					return (h->id);
				}
		}
		h = h->next;
	}
	ft_free_array(slt, n);
	return (NULL);
}

void		move_start_ants(t_room *r, t_room *head, int n)
{
	char	*dst;
	char	*name;

	while (r->type == 1)
	{
		if (r->pop > 0 && !r->moveb)
		{
			if ((dst = iterate_move(head, r)))
			{
				name = ft_itoa(n - r->pop + 1);
				ft_printf(5, "L[", name, "-", dst, "] ");
				free(name);
				if (r->type == 1)
					r->pop--;
				else
					r->pop = 0;
			}
			else
				return ;
		}
		else
			return ;
	}
	if (r->type == 1)
		ft_putstr("\n");
}

void		move_process_result(t_room *head, t_room *r, int n)
{
	char	*dst;
	char	*name;

	move_start_ants(r, head, n);
	if (r->pop > 0 && !r->moveb)
		if ((dst = iterate_move(head, r)))
		{
			name = ft_itoa(n - r->pop + 1);
			ft_printf(5, "L[", name, "-", dst, "] ");
			free(name);
			if (r->type == 1)
				r->pop--;
			else
				r->pop = 0;
		}
}

void		move_ants(t_room *r, int n)
{
	t_room	*head;

	head = r;
	head->moveb = 1;
	while (move_pos(head))
	{
		reset(head);
		r = head;
		while (r)
		{
			move_process_result(head, r, n);
			r = r->next;
		}
		ft_putstr("\n");
	}
	if (populated(head))
		move_ants(head, n);
}
