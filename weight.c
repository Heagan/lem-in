/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   weight.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 15:30:43 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/14 15:30:44 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void		set_start_weight(t_room *h)
{
	while (h)
	{
		if (h->type == 1)
			h->weight = 1000;
		h = h->next;
	}
}

void		set_weight(t_room *h, t_room *r, int w)
{
	char	**slt;
	int		n;
	int		b;

	if (r->weight != -1 && r->weight <= w)
		return ;
	r->weight = w;
	slt = ft_strnsplit(r->links, ' ', &n);
	r = h;
	while (r)
	{
		b = n + 1;
		while (--b > 0)
			if (ft_strcmp(slt[b - 1], r->id) == 0)
				set_weight(h, r, w + 1);
		r = r->next;
	}
	ft_free_array(slt, n);
}

void		calc_weight(t_room *r)
{
	t_room *head;

	head = r;
	while (r)
	{
		if (r->type == 2)
			set_weight(head, r, 0);
		r = r->next;
	}
	set_start_weight(head);
}
