/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   node.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 15:05:51 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/14 15:05:57 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void		initstruct(t_room *s, char **slt, int type)
{
	s->id = ft_strdup(slt[0]);
	s->x = ft_atoi(slt[1]);
	s->y = ft_atoi(slt[2]);
	s->type = type;
	s->weight = -1;
	s->pop = 0;
	s->links = ft_strnew(50);
	s->moveb = 0;
	s->prev = "";
	s->next = NULL;
}

void		appened_list(t_room **r, t_room *s)
{
	t_room	*tmp;

	tmp = *r;
	if (!*r)
	{
		*r = s;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = s;
}

void		addnode(t_room **r, char *info, int type)
{
	t_room	*s;
	char	**slt;
	int		n;

	s = (t_room *)malloc(sizeof(t_room));
	slt = ft_strnsplit(info, ' ', &n);
	if (n != 3)
		ft_error("Error reading map!", *r);
	initstruct(s, slt, type);
	appened_list(r, s);
	ft_free_array(slt, 3);
}

void		addlink(t_room **r, char **slt)
{
	t_room	*tmp;
	int		succeded;

	tmp = *r;
	succeded = 0;
	while (tmp && !succeded)
	{
		if (ft_strcmp(tmp->id, slt[0]) == 0)
		{
			tmp->links = ft_strend(tmp->links, slt[1]);
			tmp->links = ft_strend(tmp->links, " ");
			succeded = 1;
		}
		tmp = tmp->next;
	}
	if (!succeded)
		ft_error("File Error!\nA link couldn't be made!\nCheck connections!",
		*r);
}
