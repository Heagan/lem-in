/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 15:12:05 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/15 09:54:42 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void		check_valid(t_room *h)
{
	int		s;
	int		e;

	s = 0;
	e = 0;
	while (h)
	{
		if (h->type == 1)
		{
			if (ft_strlen(h->links) == 0)
				ft_error("Map error!\nStart isn't linked up!", h);
			s++;
		}
		if (h->type == 2)
		{
			if (ft_strlen(h->links) == 0)
				ft_error("Map error!\nEnd isn't linked up!", h);
			e++;
		}
		h = h->next;
	}
	if (s != 1)
		ft_error("Map doesn't have a start properly defined!", h);
	if (e != 1)
		ft_error("Map doesn't have a end properly defined!", h);
}

int			is_end_start(char *id, t_room *h, t_room *r)
{
	while (h)
	{
		if (ft_strequ(id, h->id))
			if (h->type == 2)
				return (1);
		if (r->type == 1)
			return (1);
		h = h->next;
	}
	return (0);
}

int			populated(t_room *h)
{
	while (h)
	{
		if (h->pop && h->type != 2)
			return (1);
		h = h->next;
	}
	return (0);
}
