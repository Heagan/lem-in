/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fants.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 15:09:09 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/14 15:20:38 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int			ants(void)
{
	int		ant_num;
	char	*str;

	str = NULL;
	if (get_next_line(0, &str) == -1)
		ft_error("Please specify a map file not folder!", NULL);
	while (str[0] == '#')
	{
		ft_putendl(str);
		free(str);
		get_next_line(0, &str);
	}
	ant_num = ft_atoi(str);
	if (!ft_isnumber(str))
	{
		free(str);
		ft_error("Invalid Map file\nDoesn't contain number of ants!", NULL);
	}
	if (ant_num <= 0)
	{
		free(str);
		ft_error("Map doesn't have enough ants!", NULL);
	}
	free(str);
	return (ant_num);
}
