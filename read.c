/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 15:08:05 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/14 15:08:23 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void		readnewnode(t_room **head, char *str)
{
	char	*line;

	if (ft_strequ(str, "##start"))
	{
		get_next_line(0, &line);
		addnode(head, line, 1);
		ft_putendl(line);
		free(line);
	}
	else if (ft_strequ(str, "##end"))
	{
		get_next_line(0, &line);
		addnode(head, line, 2);
		ft_putendl(line);
		free(line);
	}
	else
		addnode(head, str, 0);
}

void		readlinks(t_room **head, char *str)
{
	char	**slt;
	char	*t;
	int		n;

	slt = ft_strnsplit(str, '-', &n);
	if (n != 2)
		ft_error("Error reading map!\nCheck your links!", *head);
	addlink(head, slt);
	t = slt[0];
	slt[0] = slt[1];
	slt[1] = t;
	addlink(head, slt);
	ft_free_array(slt, n);
}

int			readfile(t_room **head)
{
	char	*str;

	str = NULL;
	while (get_next_line(0, &str))
	{
		ft_putendl(str);
		while (str[0] == '#' && str[1] != '#')
		{
			free(str);
			get_next_line(0, &str);
			ft_putendl(str);
		}
		if (!ft_strchr(str, '-'))
			readnewnode(head, str);
		else
			readlinks(head, str);
		free(str);
	}
	free(str);
	return (1);
}
