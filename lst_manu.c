/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_manu.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 15:30:03 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/14 15:34:02 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void		reset(t_room *h)
{
	while (h)
	{
		h->moveb = 0;
		h = h->next;
	}
}

t_room		*set_start(t_room *h, int n)
{
	t_room	*r;
	t_room	*tmp;

	r = h;
	while (r->next)
	{
		if (r->type == 1)
			r->pop = n;
		if (r->next->type == 1)
		{
			r->next->pop = n;
			if (!ft_strequ(r->next->id, h->id))
			{
				tmp = r->next->next;
				r->next->next = h;
				h = r->next;
				r->next = tmp;
			}
			return (h);
		}
		r = r->next;
	}
	return (h);
}

void		free_head(t_room *h)
{
	t_room	*tmp;

	tmp = h;
	while (h)
	{
		tmp = h->next;
		free(h->id);
		free(h->links);
		free(h);
		h = tmp;
	}
}

void		get_end(t_room *h, int n)
{
	t_room	*head;

	head = h;
	while (h)
	{
		if (h->type == 2)
			if (h->pop != n)
				ft_error(TES, h);
		h = h->next;
	}
}
