/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 15:37:17 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/14 15:38:09 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# include <stdlib.h>
# include <fcntl.h>
# include "libft/libft.h"

# define TES "Invalid Map!\nIt looks like not all the pieces reached the end!"

typedef struct		s_room
{
	char			*id;
	char			*prev;
	int				type;
	int				x;
	int				y;
	int				weight;
	int				pop;
	char			*links;
	int				moveb;
	struct s_room	*next;
}					t_room;

int					populated(t_room *h);
void				reset(t_room *h);
void				get_end(t_room *h, int n);
t_room				*set_start(t_room *h, int n);
void				check_valid(t_room *h);
void				ft_free_array(char **array, size_t size);
void				free_head(t_room *h);
void				move_ants(t_room *r, int n);
void				addnode(t_room **r, char *info, int type);
void				addlink(t_room **r, char **slt);
void				ft_error(char *e, t_room *h);
int					ants(void);
int					readfile(t_room **head);
int					ft_isnumber(char *num);
void				calc_weight(t_room *r);

#endif
