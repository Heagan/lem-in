/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 15:11:22 by gsferopo          #+#    #+#             */
/*   Updated: 2017/11/15 10:08:48 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int			main(void)
{
	t_room	*head;
	int		ant_num;

	head = NULL;
	ant_num = ants();
	ft_putnbrl(ant_num);
	readfile(&head);
	check_valid(head);
	calc_weight(head);
	head = set_start(head, ant_num);
	ft_putendl("");
	move_ants(head, ant_num);
	ft_putendl("");
	get_end(head, ant_num);
	free_head(head);
	return (0);
}
