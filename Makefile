NAME = lem-in

SRC = *.c

FLAGS = -Wall -Wextra -Werror -g -Wuninitialized

LIBFT = ./libft/libft.a

INCLUDES = -I lemin.h

all:
	make -C libft all
	gcc -o $(NAME) $(SRC) $(LIBFT) $(FLAGS)

clean:
	rm -f $(NAME)

fclean: clean

re: fclean all
